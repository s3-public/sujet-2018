package start;

public class CompetencesDepartements extends Competences{
	
	public CompetencesDepartements() {
		this.getCompetences().put("Enseignement","Coll�ges (b�timents, personnels ouvriers, techniciens et de service)");
		this.getCompetences().put("Culture et vie sociale","�ducation, cr�ation, biblioth�ques, mus�es, archives");
		this.getCompetences().put("Action sociale et m�dico-sociale","protection maternelle et infantile, aide sociale � l�enfance");
		this.getCompetences().put("Am�nagement du territoir","Sch�ma r�gional (avis, approbation)");
		this.getCompetences().put("Environnement","Espaces naturels, D�chets (plan d�partemental), participation au sch�ma d�am�nagement et de gestion des eaux");
		this.getCompetences().put("Grands �quipements","Ports maritimes, de commerce et de p�che, A�rodromes");
		this.getCompetences().put("D�veloppement �conomique","Aides indirectes");
		this.getCompetences().put("S�curit�","Circulation, Pr�vention de la d�linquance, Incendie et secours");
	}
	
	@Override
	public void rechercherUneCompetence(String competence) {
		System.out.print("D�partement : ");
		super.rechercherUneCompetence(competence);
	}
	
	@Override
	public void afficherCompetencesDomaine(String domaine) {
		System.out.print("D�partement : ");
		super.afficherCompetencesDomaine(domaine);
	}

}
