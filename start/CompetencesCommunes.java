package start;

public class CompetencesCommunes extends Competences{
	
	public CompetencesCommunes() {
		this.getCompetences().put("Enseignement","�coles (b�timents)");
		this.getCompetences().put("Culture et vie sociale","�ducation, cr�ation, biblioth�ques, mus�es, archives");
		this.getCompetences().put("Enfance","cr�ches, centres de loisirs");
		this.getCompetences().put("sports et loisirs","�quipements et subventions, tourisme");
		this.getCompetences().put("Action sociale et m�dico-sociale","CCAS : centre communal d�action sociale");
		this.getCompetences().put("Urbanisme","plan local d�urbanisme, sch�ma de coh�rence territoriale, permis de construire, zone d�am�nagement concert�");
		this.getCompetences().put("Am�nagement du territoir","Sch�ma r�gional (avis, approbation)");
		this.getCompetences().put("Environnement","Espaces naturels, collecte et traitement des d�chets, Eau (distribution, assainissement), �nergie (distribution)");
		this.getCompetences().put("Grands �quipements","Ports de plaisance, A�rodromes");
		this.getCompetences().put("D�veloppement �conomique","Aides indirectes");
		this.getCompetences().put("S�curit�","Police municipale, Circulation et stationnement, Pr�vention de la d�linquance");
	}
	
	@Override
	public void rechercherUneCompetence(String competence) {
		System.out.print("Commune : ");
		super.rechercherUneCompetence(competence);
	}
	
	@Override
	public void afficherCompetencesDomaine(String domaine) {
		System.out.print("Commune : ");
		super.afficherCompetencesDomaine(domaine);
	}
}
