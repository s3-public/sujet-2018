package start;

import java.util.HashMap;
import java.util.Map;

public abstract class Competences {
	
	private Map <String,String> competences;
	private Competences suivant;
	
	public Competences() {
		competences = new HashMap<String,String>();
	}
	
	public void setSuivant(Competences suivant) {
		this.suivant = suivant;
	}
	
	public Map<String, String> getCompetences() {
		return competences;
	}
	
	public void rechercherUneCompetence(String competence) {
		boolean rien = true;
		for (String s:this.competences.values()) {
			String competences = s.toLowerCase();
			if (competences.contains(competence)) {
				System.out.print(s);
				rien = false;
			}				
		}
		if (rien)
			System.out.println("-");
		else
			System.out.println();
		if(suivant !=null) {
			suivant.rechercherUneCompetence(competence);
		}
	}
	
	public void afficherCompetencesDomaine(String domaine) {
		String competences = this.competences.get(domaine);
		if (competences == null)
			System.out.println("-");
		else
			System.out.println(competences);
		if(suivant !=null) {
			suivant.afficherCompetencesDomaine(domaine);
		}
	}

}
