package start;

public class Main {

	public static void main(String[] args) {
		GestionDesCompetences france = new GestionDesCompetences();
		
		france.rechercherUneCompetenceDuBasVersLeHaut("police");
		System.out.println();
		france.rechercherUneCompetenceDuBasVersLeHaut("a�rodrome");
		System.out.println();
		france.rechercherUneCompetenceDuBasVersLeHaut("handicap");
		System.out.println();
		france.rechercherUneCompetenceDuHautVersLeBas("port");
		System.out.println();
		france.rechercherUneCompetenceDuHautVersLeBas("universit�");
		System.out.println();
		france.afficherCompetencesDomaineDuBasVersLeHaut("Enseignement");
		System.out.println();
		france.afficherCompetencesDomaineDuBasVersLeHaut("Urbanisme");
	}
}
