package start;

public class GestionDesCompetences {
	
	private Competences communes;
	private Competences departements;
	private Competences regions;
	private Competences etat;
	
	
	public GestionDesCompetences() {
		//création des maillons
		this.communes= new CompetencesCommunes();
		this.departements = new CompetencesDepartements();
		this.regions = new CompetencesRegions();
		this.etat = new CompetencesEtat();
	}
	
	public void rechercherUneCompetenceDuBasVersLeHaut(String competence) {
		System.out.println("Recherche de la responsabilité de la compétence " + competence);
		
		//definition des suivants
		this.communes.setSuivant(departements);
		this.departements.setSuivant(regions);
		this.regions.setSuivant(etat);
		this.etat.setSuivant(null);
		
		//appel du premier maillon
		this.communes.rechercherUneCompetence(competence);
	}

	public void rechercherUneCompetenceDuHautVersLeBas(String competence) {
		System.out.println("Recherche de la responsabilité de la compétence " + competence);
		
		//definition des suivants
		this.etat.setSuivant(regions);
		this.regions.setSuivant(departements);
		this.departements.setSuivant(communes);
		this.communes.setSuivant(null);
		
		//appel du premier maillon
		this.etat.rechercherUneCompetence(competence);
	}
	
	public void afficherCompetencesDomaineDuBasVersLeHaut(String domaine) {
		System.out.println("afficher les compétences du domaine " + domaine);
		
		//definition des suivants
		this.communes.setSuivant(departements);
		this.departements.setSuivant(regions);
		this.regions.setSuivant(etat);
		this.etat.setSuivant(null);
		
		//appel du premier maillon
		this.communes.afficherCompetencesDomaine(domaine);
	}

}
