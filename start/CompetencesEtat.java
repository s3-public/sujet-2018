package start;

public class CompetencesEtat extends Competences{
	
	public CompetencesEtat() {
		this.getCompetences().put("Formation Professionnelle et Apprentissage","D�finition de la politique nationale et mise en �uvre pour certains publics");
		this.getCompetences().put("Enseignement","Universit�s (b�timents, personnel), Politique �ducative");
		this.getCompetences().put("Culture et vie sociale","patrimoine, �ducation, cr�ation, biblioth�ques, mus�es, archives");
		this.getCompetences().put("sports et loisirs","formation, subventions, tourisme");
		this.getCompetences().put("Action sociale et m�dico-sociale","allocation d�adulte handicap�, centre d�h�bergement et de r�insertion sociale");
		this.getCompetences().put("Urbanisme","projet d�int�r�t g�n�ral, op�rations d�int�r�t national, directive territoriale d�am�nagement");
		this.getCompetences().put("Am�nagement du territoir","politique d'am�nagement du territoir, contrat de projet �tat/r�gion");
		this.getCompetences().put("Environnement","Espaces naturels, Parcs Nationaux, sch�ma d�am�nagement et de gestion des eaux, �nergie");
		this.getCompetences().put("Grands �quipements","Ports autonomes et d�int�r�t national, Voies navigables, A�rodromes");
		this.getCompetences().put("D�veloppement �conomique","Politique �conomique");
		this.getCompetences().put("S�curit�","Police g�n�rale et polices sp�ciales");
	}

	@Override
	public void rechercherUneCompetence(String competence) {
		System.out.print("Etat: ");
		super.rechercherUneCompetence(competence);
	}
	
	@Override
	public void afficherCompetencesDomaine(String domaine) {
		System.out.print("Etat: ");
		super.afficherCompetencesDomaine(domaine);
	}
}
