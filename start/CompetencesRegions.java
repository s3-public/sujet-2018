package start;

public class CompetencesRegions extends Competences{
	
	public CompetencesRegions() {
		this.getCompetences().put("Formation Professionnelle et Apprentissage","D�finition de la politique r�gionale et mise en �uvre");
		this.getCompetences().put("Enseignement","Lyc�es (b�timents, personnels ouvriers, techniciens et de service)");
		this.getCompetences().put("Culture et vie sociale","patrimoine, �ducation, cr�ation, biblioth�ques, mus�es, archives");
		this.getCompetences().put("sports et loisirs","subventions, tourisme");
		this.getCompetences().put("Am�nagement du territoir","Sch�ma r�gional (�laboration), contrat de projet �tat/r�gion");
		this.getCompetences().put("Environnement","Espaces naturels, Parcs R�gionaux, participation au sch�ma d�am�nagement et de gestion des eaux");
		this.getCompetences().put("Grands �quipements","Ports fluviaux, A�rodromes");
		this.getCompetences().put("D�veloppement �conomique","Aides directes et indirectes");	
	}
	
	@Override
	public void rechercherUneCompetence(String competence) {
		System.out.print("R�gion : ");
		super.rechercherUneCompetence(competence);
	}
	
	@Override
	public void afficherCompetencesDomaine(String domaine) {
		System.out.print("R�gion : ");
		super.afficherCompetencesDomaine(domaine);
	}

}
